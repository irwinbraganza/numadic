package main

import (
	"bufio"
	"flag"
	"log"
	"net"
	"reflect"
	"strconv"
	"strings"
	"time"
	"fmt"
	"github.com/shirou/gopsutil/cpu"
    "github.com/shirou/gopsutil/mem"
    "math/rand"
    "encoding/json"
    "net/http"
    "bytes"
    "crypto/tls"
    "crypto/x509"
    "io/ioutil"
)

var server_url = "https://mockserver.com:3000"

var (
	certFile = flag.String("cert", "../../../cert/server.crt", "A PEM eoncoded certificate file.")
	keyFile  = flag.String("key", "../../../cert/server.key", "A PEM encoded private key file.")
	caFile   = flag.String("CA", "../../../cert/server.crt", "A PEM eoncoded CA's certificate file.")
)

type client struct {
	Conn    net.Conn
	Name    string
	Message chan string
	Room    string
	Opened	bool
}

type chatRoom struct {
	name     string
	messages chan string
	members  map[string]*client
}

var flagIP = flag.String("ip", "127.0.0.1", "IP address to listen on")
var flagPort = flag.String("port", "8181", "Port to listen on")
var customTime = "02/01/2006 15:04:05"
var help = map[string]string{
	"\\quit":      "quit\n",
	"\\listrooms": "list all online users\n",
	"\\create":    "create a new room\n",
	"\\join":      "join a room\n",
	"\\help":      "prints all available commands\n",
}
var roomList = map[string]*chatRoom{}

func main() {
	flag.Parse()

	//start listener
	listener, err := net.Listen("tcp", *flagIP+":"+*flagPort)
	if err != nil {
		log.Fatalf("could not listen on interface %v:%v error: %v ", *flagIP, *flagPort, err)
	}
	defer listener.Close()
	log.Println("listening on: ", listener.Addr())

	//main listen accept loop
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalf("could not accept connection %v ", err)
		}
		//create new client on connection
		go createclient(conn)
	}
}

func createclient(conn net.Conn) {

	log.Printf("createclient: remote connection from: %v", conn.RemoteAddr())

	name, err := readInput(conn, "Please Enter Name: ")
	if err != nil {
		panic(err)
	}

	writeFormattedMsg(conn, "Welcome "+name)



	//init client struct
	client := &client{
		Message: make(chan string),
		Conn:    conn,
		Name:    name,
		Room:    "",
		Opened:	true,
	}

	log.Printf("new client created: %v %v", client.Conn.RemoteAddr(), client.Name)
	
	go client.trackDevice(10*time.Second)
	go client.trackDevice(1*time.Second)
	
	//spin off seperate send, recieve
	go client.send()
	go client.recieve()

	//print help
	writeFormattedMsg(conn, help)
}

func (c *client) close() {
	c.Opened = false
	c.leave()
	c.Conn.Close()
	c.Message <- "\\quit"
}

func (c *client) trackDevice(secs time.Duration) {
    for range time.Tick(secs) {
        if secs == 1*time.Second {
            pingServerEverySec(c)
        }else{
        	if c.Opened == false {
				pingServer(c, 0)
				break
			}
            pingServer(c, 1)
        }
        fmt.Println("Client Device Identity:", c.Conn.RemoteAddr().String())
    }
}

func dealwithErr(err error) {
    if err != nil {
        fmt.Println(err)
    }
}

func pingServer(c *client, state int) {
    status := make(map[int]string)
    status[0] = "0x00"
    status[1] = "0x01"

    latitude := make(map[int]string)
    latitude[0] = "15.4537162" //odxel
    latitude[1] = "15.4815283" //Talegoa
    latitude[2] = "15.4647522" //Bambolim
    latitude[3] = "15.4608099" // Dona Paula
    latitude[4] = "15.46911969999999" //Santa Cruz
    latitude[5] = "15.2832187" //Margao
    latitude[6] = "15.2281854" //Quepem
    latitude[7] = "15.3860329" //Vasco

	longitude := make(map[int]string)
    longitude[0] = "73.82986800000003" //odxel
    longitude[1] = "73.82197410000003" //Talegoa
    longitude[2] = "73.8624893" //Bambolim
    longitude[3] = "73.80955740000002" //Dona Paula
    longitude[4] = "73.8448469" //Santa Cruz
    longitude[5] = "73.98619099999996" //Margao
    longitude[6] = "74.0646984" //Quepem
    longitude[7] = "73.84403980000002" //Vasco

    rand_geo_key := rand.Intn(8)

    device_data := make(map[string]string)

    device_data["device_id"] = c.Conn.RemoteAddr().String()
    device_data["latitude"] = latitude[rand_geo_key]
    device_data["longitude"] = longitude[rand_geo_key]
    device_data["created_date"] = strconv.FormatInt(time.Now().Unix(), 10)
    device_data["status"] = status[state]
    device_data["speed"] = strconv.Itoa(rand.Intn(100))
    firePostRequest("/devicelocation", device_data)
}

func pingServerEverySec(c *client) {
    device_data := make(map[string]string)

    percentage, err := cpu.Percent(0, false)
    dealwithErr(err)

    vmStat, err := mem.VirtualMemory()
    dealwithErr(err)

    device_data["device_id"] = c.Conn.RemoteAddr().String()
    device_data["cpu_usage_percent"] = strconv.FormatFloat(percentage[0], 'f', 2, 64)
    device_data["ram_usage_percent"] = strconv.FormatFloat(vmStat.UsedPercent, 'f', 2, 64)
    device_data["created_date"] = strconv.FormatInt(time.Now().Unix(), 10)
    firePostRequest("/devicehealth", device_data)

}

func firePostRequest(url string, device_data map[string]string) {
	data, _ := json.Marshal(device_data)
	fmt.Printf("%s", data)
	// Load client cert
	cert, err := tls.LoadX509KeyPair(*certFile, *keyFile)
	if err != nil {
		log.Fatal(err)
	}
	// Load CA cert
	caCert, err := ioutil.ReadFile(*caFile)
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// Setup HTTPS client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
	}
	tlsConfig.BuildNameToCertificate()
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	sslclient := &http.Client{Transport: transport}
	resp, err := sslclient.Post(server_url+url, "application/json", bytes.NewBuffer(data))
	fmt.Println(resp)
	fmt.Println(err)
}

func (c *client) recieve() {
	for {
		msg := <-c.Message
		if msg == "\\quit" {
			break
		}
		log.Printf("recieve: client(%v) recvd msg: %s ", c.Conn.RemoteAddr(), msg)
		writeFormattedMsg(c.Conn, msg)
	}
}

func (c *client) send() {
Loop:
	for {
		msg, err := readInput(c.Conn, "")
		if err != nil {
			panic(err)
		}

		if msg == "\\quit" {
			c.close()
			log.Printf("%v has left..", c.Name)
			break Loop
		}

		if c.command(msg) {
			log.Printf("send: msg: %v from: %s", msg, c.Name)
			send := time.Now().Format(customTime) + " * (" + c.Name + "): \"" + msg + "\""

			for _, v := range roomList {
				for k := range v.members {
					if k == c.Conn.RemoteAddr().String() {
						v.messages <- send
					}
				}
			}

		} //validate
	} //for
} //end

func (c *client) command(msg string) bool {
	switch {
	case msg == "\\listrooms":
		c.Conn.Write([]byte("-------------------\n"))
		for k := range roomList {
			count := 0
			for range roomList[k].members {
				count++
			}
			c.Conn.Write([]byte(k + " : online members(" + strconv.Itoa(count) + ")\n"))
		}
		c.Conn.Write([]byte("-------------------\n"))
		return false
	case msg == "\\join":
		c.join()
		return false
	case msg == "\\help":
		writeFormattedMsg(c.Conn, help)
		return false
	case msg == "\\create":
		c.create()
		return false
	}
	return true
}

func (c *client) join() {

	roomName, err := readInput(c.Conn, "Please enter room name: ")
	if err != nil {
		panic(err)
	}

	if cr := roomList[roomName]; cr != nil {
		cr.members[c.Conn.RemoteAddr().String()] = c

		if c.Room != "" {
			c.leave()
			cr.announce(c.Name + " has left..")
		}

		c.Room = roomName
		writeFormattedMsg(c.Conn, c.Name+" has joined "+cr.name)
		cr.announce(c.Name + " has joined!")
	} else {
		writeFormattedMsg(c.Conn, "error: could not join room")
	}
}

//leave current room
func (c *client) leave() {
	//only if room is not empty
	if c.Room != "" {
		delete(roomList[c.Room].members, c.Conn.RemoteAddr().String())
		log.Printf("leave: removing user %v from room %v: current members: %v", c.Name, c.Room, roomList[c.Room].members)
		writeFormattedMsg(c.Conn, "leaving "+c.Room)
	}
}

func (c *client) create() {

	roomName, err := readInput(c.Conn, "Please enter room name: ")
	if err != nil {
		panic(err)
	}
	//if already a member of another room, leave that one first
	if roomName != "" {
		cr := createRoom(roomName)
		cr.members[c.Conn.RemoteAddr().String()] = c

		if c.Room != "" {
			c.leave()
			roomList[c.Room].announce(c.Name + " has left..")
		}
		// set clients room to new room
		c.Room = cr.name
		// add new room to map
		roomList[cr.name] = cr
		cr.announce(c.Name + " has joined!")

		writeFormattedMsg(c.Conn, "* room "+cr.name+" has been created *")
	} else {
		writeFormattedMsg(c.Conn, "* error: could not create room \""+roomName+"\" *")
	}
}

func writeFormattedMsg(conn net.Conn, msg interface{}) error {
	_, err := conn.Write([]byte("---------------------------\n"))
	t := reflect.ValueOf(msg)
	switch t.Kind() {
	case reflect.Map:
		for k, v := range msg.(map[string]string) {
			_, err = conn.Write([]byte(k + " : " + v))
		}
		break
	case reflect.String:
		v := reflect.ValueOf(msg).String()
		_, err = conn.Write([]byte(v + "\n"))
		break
	} //switch
	conn.Write([]byte("---------------------------\n"))

	if err != nil {
		return err
	}
	return nil //todo
}

func readInput(conn net.Conn, qst string) (string, error) {
	conn.Write([]byte(qst))
	s, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		log.Printf("readinput: could not read input from stdin: %v from client %v", err, conn.RemoteAddr().String())
		return "", err
	}
	s = strings.Trim(s, "\r\n")
	return s, nil
}

func createRoom(name string) *chatRoom {
	c := &chatRoom{
		name:     name,
		messages: make(chan string),
		members:  make(map[string]*client, 0),
	}
	log.Printf("creating room %v", c.name)
	//spin off new routine to listen for messages
	go func(c *chatRoom) {
		for {
			out := <-c.messages
			if out == "\\kill" {
				log.Printf("chatroom: killing \"%v\"", c.name)
				//remove from room map
				delete(roomList, c.name)
				//kill routine - only if no members
				break
			}
			for _, v := range c.members {
				v.Message <- out
				log.Printf("createroom: broadcasting msg in room: %v to member: %v", c.name, v.Name)
			}
		}
	}(c)

	//poll member list and clean up after members = 0
	go func(c *chatRoom) {
		for {
			if len(c.members) == 0 {
				log.Printf("chatroom: zero members cleaning chatroom \" %v \"", c.name)
				c.messages <- "\\kill"
				break
			}
		}
	}(c)
	return c
}

func (c *chatRoom) announce(msg string) {
	c.messages <- "* " + msg + " *"
}