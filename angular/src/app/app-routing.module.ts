import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DevicesComponent }      from './devices/devices.component';
import { ListDevicesComponent }  from './list-devices/list-devices.component';
import { TypeOneDataComponent } from './type-one-data/type-one-data.component';
import { GeoOverspeedingComponent } from './geo-overspeeding/geo-overspeeding.component';
import { GeoDwellComponent } from './geo-dwell/geo-dwell.component';
import { StationaryFilterComponent } from './stationary-filter/stationary-filter.component';

//Routes implementation
const routes: Routes = [
  { path: '', redirectTo: '/local-system-health', pathMatch: 'full' },
  { path: 'local-system-health', component: DevicesComponent },
  { path: 'list-devices', component: ListDevicesComponent },
  { path: 'type-one-data', component: TypeOneDataComponent },
  { path: 'geo-overspeeding', component: GeoOverspeedingComponent },
  { path: 'geo-dwell', component: GeoDwellComponent },
  { path: 'stationary-filter', component: StationaryFilterComponent },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}