import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoOverspeedingComponent } from './geo-overspeeding.component';

describe('GeoOverspeedingComponent', () => {
  let component: GeoOverspeedingComponent;
  let fixture: ComponentFixture<GeoOverspeedingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoOverspeedingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoOverspeedingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
