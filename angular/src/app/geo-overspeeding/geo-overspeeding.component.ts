import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-geo-overspeeding',
  templateUrl: './geo-overspeeding.component.html',
  styleUrls: ['./geo-overspeeding.component.css']
})
export class GeoOverspeedingComponent implements OnInit {

  devices: Devices[];

  constructor(private devicesService: DevicesService) {}

  ngOnInit() {}

  onSubmit(f: NgForm) {
    var req = f.value;
    this.devicesService.getGeoOverspeed(req.starttime, req.stoptime)
      .subscribe(devices => this.devices = devices);
  }

}
