import { Injectable } from '@angular/core';
import { Devices } from './devices';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class DevicesService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private devicesUrl = 'http://localhost:4000'; // URL to web api

  private log(message: string) {
    this.messageService.add('DevicesService: ' + message);
  }

  getDevices(starttime, stoptime): Observable < Devices[] > {
    this.messageService.add('DevicesService: local system heath fetched');
    var data = { startdate: starttime, enddate: stoptime };
    var config = {
      params: data,
      headers: { 'Accept': 'application/json' }
    };
    return this.http.get < Devices[] > (this.devicesUrl + '/devicehealth', config)
      .pipe(
        tap(devices => this.log(`fetched devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  listDevices(): Observable < Devices[] > {
    this.messageService.add('DevicesService: listed devices');
    return this.http.get < Devices[] > (this.devicesUrl + '/alldevices')
      .pipe(
        tap(devices => this.log(`fetched devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  getTypeOneDevices(starttime, stoptime, deviceid): Observable < Devices[] > {
    this.messageService.add('DevicesService: local system heath fetched');
    var data = { startdate: starttime, enddate: stoptime, device_id: deviceid };
    var config = {
      params: data,
      headers: { 'Accept': 'application/json' }
    };
    return this.http.get < Devices[] > (this.devicesUrl + '/devicelocation', config)
      .pipe(
        tap(devices => this.log(`fetched TypeOne Devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  getGeoOverspeed(starttime, stoptime): Observable < Devices[] > {
    this.messageService.add('DevicesService: local system heath fetched');
    var data = { startdate: starttime, enddate: stoptime };
    var config = {
      params: data,
      headers: { 'Accept': 'application/json' }
    };
    return this.http.get < Devices[] > (this.devicesUrl + '/deviceoverspeed', config)
      .pipe(
        tap(devices => this.log(`overspeed devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  getStationaryDevices(starttime, stoptime): Observable < Devices[] > {
    this.messageService.add('DevicesService: local system heath fetched');
    var data = { startdate: starttime, enddate: stoptime };
    var config = {
      params: data,
      headers: { 'Accept': 'application/json' }
    };
    return this.http.get < Devices[] > (this.devicesUrl + '/devicestationary', config)
      .pipe(
        tap(devices => this.log(`overspeed devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  getDevicesInRadius(lng, lat, starttime, stoptime): Observable < Devices[] > {
    this.messageService.add('DevicesService: local system heath fetched');
    var data = { longitude: lng, latitude: lat, startdate: starttime, enddate: stoptime };
    var config = {
      params: data,
      headers: { 'Accept': 'application/json' }
    };
    return this.http.get < Devices[] > (this.devicesUrl + '/devicesearchradius', config)
      .pipe(
        tap(devices => this.log(`overspeed devices`)),
        catchError(this.handleError('getDevices', []))
      );
  }

  private handleError < T > (operation = 'operation', result ? : T) {
    return (error: any): Observable < T > => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
