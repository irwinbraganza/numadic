import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {
	devices : Devices[];

  constructor(private devicesService: DevicesService) { }

  ngOnInit() {}

  onSubmit(f: NgForm) {
    var req = f.value;
    this.devicesService.getDevices(req.starttime, req.stoptime)
      .subscribe(devices => this.devices = devices);
  }

}
