import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-stationary-filter',
  templateUrl: './stationary-filter.component.html',
  styleUrls: ['./stationary-filter.component.css']
})
export class StationaryFilterComponent implements OnInit {

  devices: Devices[];

  constructor(private devicesService: DevicesService) {}

  ngOnInit() {}

  onSubmit(f: NgForm) {
    var req = f.value;
    this.devicesService.getStationaryDevices(req.starttime, req.stoptime)
      .subscribe(devices => this.devices = devices);
  }
}
