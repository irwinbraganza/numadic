import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationaryFilterComponent } from './stationary-filter.component';

describe('StationaryFilterComponent', () => {
  let component: StationaryFilterComponent;
  let fixture: ComponentFixture<StationaryFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationaryFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationaryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
