import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoDwellComponent } from './geo-dwell.component';

describe('GeoDwellComponent', () => {
  let component: GeoDwellComponent;
  let fixture: ComponentFixture<GeoDwellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoDwellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoDwellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
