import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-geo-dwell',
  templateUrl: './geo-dwell.component.html',
  styleUrls: ['./geo-dwell.component.css']
})

export class GeoDwellComponent implements OnInit {

  devices: Devices[];

  constructor(private devicesService: DevicesService) {}

  ngOnInit() {}

  onSubmit(f: NgForm) {
    var req = f.value;
    this.devicesService.getDevicesInRadius(req.longitude, req.latitude, req.starttime, req.stoptime)
      .subscribe(devices => this.devices = devices);
  }
}
