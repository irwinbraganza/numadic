import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-list-devices',
  templateUrl: './list-devices.component.html',
  styleUrls: ['./list-devices.component.css']
})
export class ListDevicesComponent implements OnInit {
	devices : Devices[];

  constructor(private devicesService: DevicesService) { }

  ngOnInit() {
    this.devicesService.listDevices()
      .subscribe(devices => this.devices = devices);
  }

}
