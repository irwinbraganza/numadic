import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DevicesComponent } from './devices/devices.component';
import { DevicesService } from './devices.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { AppRoutingModule } from './/app-routing.module';
import { ListDevicesComponent } from './list-devices/list-devices.component';
import { TypeOneDataComponent } from './type-one-data/type-one-data.component';
import { GeoOverspeedingComponent } from './geo-overspeeding/geo-overspeeding.component';
import { GeoDwellComponent } from './geo-dwell/geo-dwell.component';
import { StationaryFilterComponent } from './stationary-filter/stationary-filter.component';


@NgModule({
  declarations: [
    AppComponent,
    DevicesComponent,
    MessagesComponent,
    ListDevicesComponent,
    TypeOneDataComponent,
    GeoOverspeedingComponent,
    GeoDwellComponent,
    StationaryFilterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    DevicesService,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
