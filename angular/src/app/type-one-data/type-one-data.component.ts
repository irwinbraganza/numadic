import { Component, OnInit } from '@angular/core';
import { Devices } from '../devices';
import { DevicesService } from '../devices.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-type-one-data',
  templateUrl: './type-one-data.component.html',
  styleUrls: ['./type-one-data.component.css']
})
export class TypeOneDataComponent implements OnInit {

  devices: Devices[];

  constructor(private devicesService: DevicesService) {}

  ngOnInit() {}

  onSubmit(f: NgForm) {
    var req = f.value;
    this.devicesService.getTypeOneDevices(req.starttime, req.stoptime, req.deviceid)
      .subscribe(devices => this.devices = devices);
  }

}
