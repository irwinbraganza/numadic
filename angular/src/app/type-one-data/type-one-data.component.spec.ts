import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeOneDataComponent } from './type-one-data.component';

describe('TypeOneDataComponent', () => {
  let component: TypeOneDataComponent;
  let fixture: ComponentFixture<TypeOneDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeOneDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeOneDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
