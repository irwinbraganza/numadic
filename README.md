# Numadic Activity Documentation

## Certificate generation

1. The Certificates have been generated and saved in /numadic/cert folder
2. The Certificate Key is /numadic/cert/server.key
3. The Main Certificate is /numadic/cert/server.crt
4. The Steps to generate it are:

N.B: I have used mockserver.com to point localhost (127.0.0.1) it can be done as https://www.imore.com/how-edit-your-macs-hosts-file-and-why-you-would-want
i.e: You need to edit /etc/hosts in your local mac and include the following line:

	127.0.0.1       mockserver.com

Step 1: Root SSL certificate :
The first step is to create a Root Secure Sockets Layer (SSL) certificate. This root certificate can then be used to sign any number of certificates you might generate for individual domains.
Generate a RSA-2048 key and save it to a file rootCA.key. This file will be used as the key to generate the Root SSL certificate. You will be prompted for a pass phrase which you’ll need to enter each time you use this particular key to generate a certificate.

	openssl genrsa -des3 -out rootCA.key 2048

You can use the key you generated to create a new Root SSL certificate. Save it to a file namedrootCA.pem. This certificate will have a validity of 1,024 days.

	openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem

Step 2: Trust the root SSL certificate : 
Before you can use the newly created Root SSL certificate to start issuing domain certificates, there’s one more step. You need to to tell your Mac to trust your root certificate so all individual certificates issued by it are also trusted.

Open Keychain Access on your Mac and go to the Certificates category in your System keychain. Once there, import the rootCA.pem using File > Import Items. Double click the imported certificate and change the “When using this certificate:” dropdown to Always Trust in the Trust section.

Step 3: Domain SSL certificate : 
The root SSL certificate can now be used to issue a certificate specifically for your local development environment located at localhost.
Create a new OpenSSL configuration file as done in  /numadic/cert/server.csr.cnf so you can import these settings when creating a certificate instead of entering them on the command line.
Create a v3.ext file in order to create a X509 v3 certificate. as done in /numadic/cert/v3ext
Create a certificate key for localhost using the configuration settings stored in server.csr.cnf. This key is stored in /numadic/cert/server.key

	openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout server.key -config <( cat server.csr.cnf )

A certificate signing request is issued via the root SSL certificate we created earlier to create a domain certificate for localhost. The output is a certificate file called server.crt

	openssl x509 -req -in server.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out server.crt -days 500 -sha256 -extfile v3.ext

You’re now ready to secure your localhost with HTTPS. Move the server.key and server.crt files to an accessible location on your server and include them when starting your server.


## Go Client(s), NodeJS Server

### Connection type : SSL/TLS with a self signed key has been archived between the client and server, using the above steps for certificate generation as follows:

The Server Key in NodeJS server is set up by adding the following lines as done in /numadic/api/server.js:

	var http = require("http"),
		https = require("https");
	var options = {
		key: fs.readFileSync("../cert/server.key"),
		cert: fs.readFileSync("../cert/server.crt")
	}
	http.createServer(app).listen(4000);
	https.createServer(options, app).listen(3000);

The Client Key in GO is set up by adding the following code as done in /numadic/go/src/tcp/multi-chat.go

	var server_url = "https://mockserver.com:3000"
	var (
		certFile = flag.String("cert", "../../../cert/server.crt", "A PEM eoncoded certificate file.")
		keyFile  = flag.String("key", "../../../cert/server.key", "A PEM encoded private key file.")
		caFile   = flag.String("CA", "../../../cert/server.crt", "A PEM eoncoded CA's certificate file.")
	)
	data, _ := json.Marshal(device_data)
	fmt.Printf("%s", data)
	// Load client cert
	cert, err := tls.LoadX509KeyPair(*certFile, *keyFile)
	if err != nil {
	log.Fatal(err)
	}
	// Load CA cert
	caCert, err := ioutil.ReadFile(*caFile)
	if err != nil {
	log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	// Setup HTTPS client
	tlsConfig := &tls.Config{
	Certificates: []tls.Certificate{cert},
	RootCAs:      caCertPool,
	}
	tlsConfig.BuildNameToCertificate()
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	sslclient := &http.Client{Transport: transport}
	resp, err := sslclient.Post(server_url+url, "application/json", bytes.NewBuffer(data))

So now the client interacts with the Server via https://mockserver.com:3000

### The Clients Spawning for Go-NodeJS:

Start the MongoDB using the following cmd in terminal:

	sudo mongod

Start the Node JS server running the following in /numadic/api:

	npm run start


In Terminal go to /numadic/go/src/tcp/ and run the following script the start the Go server:

	go run multi-chat.go

Open a new Terminal Tab and run the following to Spawn a Client:

	telnet 127.0.0.1 8181

Enter the Client Name when requested and do the above step 10 times inorder to spawn 10 clients.
There are functions for Client Device Health that runs every 1 sec and for Client Location Health that runs every 10 secs.

Data can be checked in MongoDB under database NumadicDB in localhost:27017.


## NodeJS API

N.B: 
You need to generate certificate for Postman app via https://www.getpostman.com/docs/postman/sending_api_requests/certificates with the following details:

1. Host: mockserver.com:3000
2. CRT file: path_to/numadic/api/server.crt
3. Key file: path_to/numadic/api/server.key
4. Passphrase: 12345

Also make sure mockserver.com is there in /etc/hosts.

Geo Overspeeding and Stationary Filter API are computed while creating the record which is saved in devicelocations every Sec, implementation of it was custom coded and added via 
POST https://mockserver.com:3000/devicelocation with the following data set:

For Geo Overspeeding API results, hit with the following data > 5 times incrementing 1518764900 every sec:

	{"created_date":"1518764900","device_id":"127.0.0.1:53164","latitude":"15.496777","longitude":"73.827827","speed":"79","status":"0x01"}

For Stationary Filter API results, hit with the following data > 120 times incrementing 1518764900 every sec:

	{"created_date":"1518764900","device_id":"127.0.0.1:53164","latitude":"15.496777","longitude":"73.827827","speed":"0","status":"0x01"}

I have create > 5000 points of data (devicelocations.json + devicehealths.json combined) for all the APIs. Please import Data from /numadic/devicehealths.json and /numadic/devicelocations.json collections into your MongoDB in NumadicDB Database for better API results.

APIs can be seen in https://www.getpostman.com via Import > "Import File" by importing  Numadic%20APIs.postman_collection.json OR you via Import > "Import from link" using link => https://www.getpostman.com/collections/9ca0b8eebfa3eca793dc too in postman app.


## Angular WebApp:

Go to /numadic/angular and run the following in your terminal:

	ng serve --open

The above cmd will open http://localhost:4200 in your default browser hosting the angular WebApp.
With the Above Mongo DB imported in step 3 you should be able to filter the APIs by using the values in "eg" in the input box for quick results.

The main nginx configuration for the Angular app to work would be:

	location / {
	     if ($request_method = 'OPTIONS') {
	        add_header 'Access-Control-Allow-Origin' '*';
	        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
	        #
	        # Custom headers and headers various browsers *should* be OK with but aren't
	        #
	        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
	        #
	        # Tell client that this pre-flight info is valid for 20 days
	        #
	        add_header 'Access-Control-Max-Age' 1728000;
	        add_header 'Content-Type' 'text/plain; charset=utf-8';
	        add_header 'Content-Length' 0;
	        return 204;
	     }
	     if ($request_method = 'POST') {
	        add_header 'Access-Control-Allow-Origin' '*';
	        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
	        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
	        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
	     }
	     if ($request_method = 'GET') {
	        add_header 'Access-Control-Allow-Origin' '*';
	        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
	        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
	        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
	     }
	}

since on localhost for now I have added a cors implementaion in /numadic/api/server.js as follows:

	app.use(function (req, res, next) {
	    // Website you wish to allow to connect
	    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
	    // Request methods you wish to allow
	    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	    // Request headers you wish to allow
	    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	    // Set to true if you need the website to include cookies in the requests sent
	    // to the API (e.g. in case you use sessions)
	    res.setHeader('Access-Control-Allow-Credentials', true);
	    // Pass to next layer of middleware
	    next();
	});

Thank You, I have enjoyed working on this Activity !!! Hope I could impress you guys with my work.













