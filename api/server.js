var express = require('express'),
	app = express(),
	port = process.env.PORT || 3000,
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	http = require("http"),
	https = require("https"),
	fs = require("fs");

var deviceHealth = require('./src/models/deviceHealthModel');
var deviceLocation = require('./src/models/deviceLocationModel');
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/NumadicDB'); 

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var deviceHealthroute = require('./src/routes/deviceHealthRoutes');
deviceHealthroute(app); //register deviceHealthroutes

var devicelocationroute = require('./src/routes/deviceLocationRoutes');
devicelocationroute(app); //register devicelocationroutes

var options = {
	key: fs.readFileSync("../cert/server.key"),
	cert: fs.readFileSync("../cert/server.crt")
}

http.createServer(app).listen(4000);
https.createServer(options, app).listen(port);

app.get("/", function(req, res){
	res.send("<h1>Hello Numadic !!!<h2>");
});

app.use(function(req, res) {
  res.status(200).send({url: req.originalUrl + ' not found in Numadic APIs'})
});

console.log('Numadic RESTful API server started on: ' + port);