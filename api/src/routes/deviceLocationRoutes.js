'use strict';
module.exports = function(app) {
  var deviceLocation = require('../controllers/deviceLocationController');

  // deviceLocation Routes
  app.route('/devicelocation')
    .get(deviceLocation.list_all)
    .post(deviceLocation.create_a_entry);


  app.route('/devicelocation/:deviceLocationID')
    .get(deviceLocation.read_a_entry)
    .put(deviceLocation.update_a_entry)
    .delete(deviceLocation.delete_a_entry);

  app.route('/deviceoverspeed')
    .get(deviceLocation.deviceoverspeed);

  app.route('/devicestationary')
    .get(deviceLocation.devicestationary);

  app.route('/devicesearchradius')
    .get(deviceLocation.devicesearchradius);
};