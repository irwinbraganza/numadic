'use strict';
module.exports = function(app) {
  var deviceHealth = require('../controllers/deviceHealthController');

  // deviceHealth Routes
  app.route('/devicehealth')
    .get(deviceHealth.list_all)
    .post(deviceHealth.create_a_entry);

  app.route('/devicehealth/:deviceHealthID')
    .get(deviceHealth.read_a_entry)
    .put(deviceHealth.update_a_entry)
    .delete(deviceHealth.delete_a_entry);
  
  app.route('/alldevices')
    .get(deviceHealth.list_devices_as_type);
};