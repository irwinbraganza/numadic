'use strict';


var mongoose = require('mongoose'),
  deviceHealth = mongoose.model('deviceHealth'),
  url = require('url');


// API for Local System Health
exports.list_all = function(req, res) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  if (query.enddate == "null"){
    query.enddate = "";
  }
  if (query.startdate == "null"){
    query.startdate = "";
  }
  if (query.enddate && query.enddate.toString().length != 10 && typeof query.enddate != "number") {
    throw new Error('Please add enddate as unix timestamp');
  }
  if (query.startdate && query.startdate.toString().length != 10 && typeof query.startdate != "number") {
    throw new Error('Please add startdate as unix timestamp');
  }
  var query_string = {};
  if (query.enddate || query.startdate) {
    if (!query.enddate && query.startdate) {
      query_string = {created_date:{$gte:query.startdate}}; 
    }else if(!query.startdate && query.enddate) {
      query_string = {created_date:{$lte:query.enddate}}; 
    }else{
      query_string = {$and:[{created_date:{$gte:query.startdate}},{created_date:{$lte:query.enddate}}]};     
    }
  }
  deviceHealth.find(query_string, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API for List Devices
exports.list_devices_as_type = function(req, res) {
  var devices = {};
  var deviceLocation = mongoose.model('deviceLocation');
  var promise1 = new Promise((resolve, reject) => {
    deviceLocation.find().distinct('device_id', function(error, ids) {
      devices.type1 = ids;
      resolve(devices);
    });
  });
  var promise2 = new Promise((resolve, reject) => {
    deviceHealth.find().distinct('device_id', function(error, ids) {
      devices.type2 = ids;
      resolve(devices);
    });
  });
  Promise.all([promise1, promise2]).then(function(){
    res.json(devices);    
  });
};

//API to create entry
exports.create_a_entry = function(req, res) {
  var new_task = new deviceHealth(req.body);
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API to get entry
exports.read_a_entry = function(req, res) {
  deviceHealth.findById(req.params.deviceHealthID, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API to update entry
exports.update_a_entry = function(req, res) {
  deviceHealth.findOneAndUpdate({_id: req.params.deviceHealthID}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API to delete entry
exports.delete_a_entry = function(req, res) {
  deviceHealth.remove({
    _id: req.params.deviceHealthID
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'deviceHealth successfully deleted' });
  });
};