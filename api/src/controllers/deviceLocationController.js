'use strict';


var mongoose = require('mongoose'),
  deviceLocation = mongoose.model('deviceLocation'),
  url = require('url');

//API to List Type1 Devices
exports.list_all = function(req, res) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  if (query.enddate == "null"){
    query.enddate = "";
  }
  if (query.startdate == "null"){
    query.startdate = "";
  }
  if (query.enddate && query.enddate.toString().length != 10 && typeof query.enddate != "number") {
    throw new Error('Please add enddate as unix timestamp');
  }
  if (query.startdate && query.startdate.toString().length != 10 && typeof query.startdate != "number") {
    throw new Error('Please add startdate as unix timestamp');
  }
  var query_string = {};
  if (query.enddate || query.startdate) {
    if (!query.enddate && query.startdate) {
      query_string = {created_date:{$gte:query.startdate}}; 
    }else if(!query.startdate && query.enddate) {
      query_string = {created_date:{$lte:query.enddate}}; 
    }else{
      query_string = {$and:[{created_date:{$gte:query.startdate}},{created_date:{$lte:query.enddate}}]};     
    }
  }
  if (query.device_id && query.device_id != "undefined") {
    if (query_string.$and) {
      query_string.$and.push({device_id: query.device_id});
    }else if(Object.keys(query_string).length != 0){
      query_string = {$and:[query_string]};
      query_string.$and.push({device_id: query.device_id});
    }else{
      query_string = {device_id: query.device_id};
    }
  }
  deviceLocation.find(query_string, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API for creating entry
exports.create_a_entry = function(req, res) {
  var curr_speed = req.body.speed;
  var curr_timestamp = req.body.created_date;
  var last5_timestamp = curr_timestamp - 5;
  req.body.geo = [req.body.longitude, req.body.latitude];

  if (curr_speed == 0) {
    //Implmentaion for manipulating Stationary Filter
    deviceLocation.find({$and:[{speed:0},{device_id:req.body.device_id},{created_date:curr_timestamp-1}]}, function(err, last_instance) {
      var last_instance = last_instance[0];
      var total_stationary = 1;
      if(last_instance){
        if (last_instance.stationary) {
          total_stationary = last_instance.stationary + 1;
        }        
      }
      req.body.stationary = total_stationary;
      var new_task = new deviceLocation(req.body);
      new_task.save(function(err, task) {
        if (err)
          res.send(err);
        res.json(task);
      });
    }).sort({ "created_date": -1 }); 
  }else{
    //Implmentaion for manipulating Geo OverSpeed
    var query_string_overspeeding = {$and:[{created_date:{$gte:last5_timestamp}},{created_date:{$lte:curr_timestamp}},{speed:{$gt:60}},{device_id:req.body.device_id}]};
    deviceLocation.find(query_string_overspeeding).distinct('created_date',function(err, task) {
      if (err)
        res.send(err);

      var curr_chk = 0;
      if (curr_speed > 60) {
        curr_chk = 1;
      }
      if (task.length + curr_chk > 5) {
        deviceLocation.find({$and:[{created_date:last5_timestamp},{speed:{$gt:60}},{device_id:req.body.device_id}]}, function(err, first_instance) {
          var first_instance = first_instance[0];
          req.body.overspeeding = first_instance;
          var new_task = new deviceLocation(req.body);
          new_task.save(function(err, task) {
            if (err)
              res.send(err);
            res.json(task);
          });
        });
      }else{
        var new_task = new deviceLocation(req.body);
        new_task.save(function(err, task) {
          if (err)
            res.send(err);
          res.json(task);
        });
      }
    });
  }
};

//API for Stationary Filter
exports.devicestationary = function(req, res) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  if (query.enddate == "null"){
    query.enddate = "";
  }
  if (query.startdate == "null"){
    query.startdate = "";
  }
  if (query.enddate && query.enddate.toString().length != 10 && typeof query.enddate != "number") {
    throw new Error('Please add enddate as unix timestamp');
  }
  if (query.startdate && query.startdate.toString().length != 10 && typeof query.startdate != "number") {
    throw new Error('Please add startdate as unix timestamp');
  }
  var query_string = {};
  if (query.enddate || query.startdate) {
    if (!query.enddate && query.startdate) {
      query_string = {created_date:{$gte:query.startdate}}; 
    }else if(!query.startdate && query.enddate) {
      query_string = {created_date:{$lte:query.enddate}}; 
    }else{
      query_string = {$and:[{created_date:{$gte:query.startdate}},{created_date:{$lte:query.enddate}}]};     
    }
  }
  var stationary_query = {stationary:{$gt:120}};
  if (query_string.$and) {
    query_string.$and.push(stationary_query);
  }else if(Object.keys(query_string).length != 0){
    query_string = {$and:[query_string]};
    query_string.$and.push(stationary_query);
  }else{
    query_string = stationary_query;
  }

  deviceLocation.find(query_string, function(err, task) {
      if (err)
        res.send(err);
      res.json(task);
  });
}

//API for Geo Overspeed 
exports.deviceoverspeed = function(req, res) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  if (query.enddate == "null"){
    query.enddate = "";
  }
  if (query.startdate == "null"){
    query.startdate = "";
  }
  if (query.enddate && query.enddate.toString().length != 10 && typeof query.enddate != "number") {
    throw new Error('Please add enddate as unix timestamp');
  }
  if (query.startdate && query.startdate.toString().length != 10 && typeof query.startdate != "number") {
    throw new Error('Please add startdate as unix timestamp');
  }
  var query_string = {};
  if (query.enddate || query.startdate) {
    if (!query.enddate && query.startdate) {
      query_string = {created_date:{$gte:query.startdate}}; 
    }else if(!query.startdate && query.enddate) {
      query_string = {created_date:{$lte:query.enddate}}; 
    }else{
      query_string = {$and:[{created_date:{$gte:query.startdate}},{created_date:{$lte:query.enddate}}]};     
    }
  }
  var overspeed_query = {overspeeding: {$gt: {}}};
  if (query_string.$and) {
    query_string.$and.push(overspeed_query);
  }else if(Object.keys(query_string).length != 0){
    query_string = {$and:[query_string]};
    query_string.$and.push(overspeed_query);
  }else{
    query_string = overspeed_query;
  }
  deviceLocation.find(query_string, function(err, task) {
      if (err)
        res.send(err);
      res.json(task);
  });
}

//API for Geo Dwell
exports.devicesearchradius = function(req, res) {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var lat = query.latitude;
  var lng = query.longitude;
  if (lat < -90 || lat > 90) {
      throw new Error("Latitude must be between -90 and 90 degrees inclusive.");
  }else if (lng < -180 || lng > 180) {
      throw new Error("Longitude must be between -180 and 180 degrees inclusive.");
  }else if (!lat || !lng) {
      throw new Error("Enter a valid Latitude or Longitude!");
  }else{
    var query_string = {};
    if (query.enddate == "null"){
      query.enddate = "";
    }
    if (query.startdate == "null"){
      query.startdate = "";
    }
    if (query.enddate || query.startdate) {
      if (!query.enddate && query.startdate) {
        query_string = {created_date:{$gte:query.startdate}}; 
      }else if(!query.startdate && query.enddate) {
        query_string = {created_date:{$lte:query.enddate}}; 
      }else{
        query_string = {$and:[{created_date:{$gte:query.startdate}},{created_date:{$lte:query.enddate}}]};     
      }
    }
    var geo_query = { geo : { $near : [lng,lat] , $maxDistance : 1000 / 6371 } };
    if (query_string.$and) {
      query_string.$and.push(geo_query);
    }else if(Object.keys(query_string).length != 0){
      query_string = {$and:[query_string]};
      query_string.$and.push(geo_query);
    }else{
      query_string = geo_query;
    }
    deviceLocation.find(query_string).distinct("device_id", function(err, task) {
        if (err)
          res.send(err);
        res.json(task);
    });
  }
}

//API to get entry
exports.read_a_entry = function(req, res) {
  deviceLocation.findById(req.params.deviceLocationID, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

//API to update entry
exports.update_a_entry = function(req, res) {
  req.body.geo = [req.body.longitude, req.body.latitude];
  deviceLocation.findOneAndUpdate({_id: req.params.deviceLocationID}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


//API to delete entry
exports.delete_a_entry = function(req, res) {
  deviceLocation.remove({
    _id: req.params.deviceLocationID
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'deviceLocation successfully deleted' });
  });
};
