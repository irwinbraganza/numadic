'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var deviceHealthSchema = new Schema({
  device_id: {
    type: String,
    required: 'Kindly enter the device ID'
  },
  created_date: {
    type: Number,
    default: function(){return (new Date()).getTime()/1000|0}
  },
  ram_usage_percent:{
    type: Number
  },
  cpu_usage_percent:{
    type: Number
  }
});

module.exports = mongoose.model('deviceHealth', deviceHealthSchema);