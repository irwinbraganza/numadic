'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var deviceLocationSchema = new Schema({
  device_id: {
    type: String,
    required: 'Kindly enter the device ID'
  },
  geo: {
    type: [Number],
    index: '2d'
  },
  created_date: {
    type: Number,
    default: function(){return (new Date()).getTime()/1000|0}
  },
  status:{
    type: String
  },
  speed:{
    type: Number
  },
  overspeeding:{
    type: Schema.Types.Mixed
  },
  stationary:{
    type: Number
  }
});

module.exports = mongoose.model('deviceLocation', deviceLocationSchema);